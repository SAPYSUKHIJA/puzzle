//
//  ViewController.swift
//  Task16March
//
//  Created by Sierra 4 on 16/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let imagePicker = UIImagePickerController()
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnImgPicker: UIButton!
    @IBOutlet var ImageView: UIImageView!
        {
        didSet{
            
                ImageView.image = #imageLiteral(resourceName: "nature")
                ImageView.clipsToBounds = true
              }
    }

       override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self

        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func btNImgPassed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ImagePassed", sender: self)
    }
   
    @IBAction func btnImgChoosed(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination as! ViewControllerTiles
        viewController.ImagePassed = ImageView.image
    }


}

extension ViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            ImageView.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }

}

    

 










   
  
