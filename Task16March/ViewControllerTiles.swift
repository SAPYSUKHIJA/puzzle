//
//  ViewControllerTiles.swift
//  Task16March
//
//  Created by Sierra 4 on 16/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import CLTimer

class ViewControllerTiles: UIViewController {

    @IBOutlet var Timer: CLTimer!
    @IBOutlet var ImgesCollection: [UIImageView]!
    var ImagePassed:UIImage!
     var array2 = [Int]()
    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.startTimer(withSeconds: 5, format:.Minutes , mode: .Reverse)
        let topHalf = ImagePassed.topHalf
        let MiddleHalf = ImagePassed.MiddleHalf
        let bottomHalf = ImagePassed.bottomHalf
        let topLeft = topHalf?.leftHalf
        let topMiddle = topHalf?.horizontallyMiddleHalf
        let topRight = topHalf?.rightHalf
        let centerLeft = MiddleHalf?.leftHalf
        let centerMiddle = MiddleHalf?.horizontallyMiddleHalf
        let centerRight = MiddleHalf?.rightHalf
        let bottomLeft = bottomHalf?.leftHalf
        let bottomMiddle = bottomHalf?.horizontallyMiddleHalf
        let bottomRight = bottomHalf?.rightHalf
       // var array = [0,1,2,3,4,5,6,7,8]
        var imagesArray = [topLeft,topMiddle,topRight,centerLeft,centerMiddle,centerRight,bottomLeft,bottomMiddle,bottomRight]
        imagesArray.shuffle()
        ImgesCollection[0].image = imagesArray[0]
        ImgesCollection[1].image = imagesArray[1]
        ImgesCollection[2].image = imagesArray[2]
        ImgesCollection[3].image = imagesArray[3]
        ImgesCollection[4].image = imagesArray[4]
        ImgesCollection[5].image = imagesArray[5]
        ImgesCollection[6].image = imagesArray[6]
        ImgesCollection[7].image = imagesArray[7]
        ImgesCollection[8].image = imagesArray[8]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    @IBAction func Back_Btn(_ sender: UIButton) {
          _=self.navigationController?.popViewController(animated: true)
    }    
}

extension UIImage {
    var topHalf: UIImage? {
        guard let cgImage = cgImage,
            let image = cgImage.cropping(to: CGRect(
                origin: .zero,
                size: CGSize(width: size.width, height: size.height/3)
                )
            )
            else
        {
            return nil
        }
        return UIImage(cgImage: image, scale: 1, orientation: imageOrientation)
    }
    
    var MiddleHalf: UIImage? {
        guard let cgImage = cgImage,
            let image = cgImage.cropping(to: CGRect(
                origin: CGPoint(x: 0,  y: CGFloat(Int(size.height)-Int(size.height/3)-Int(size.height/3))),
                size: CGSize(width: size.width, height: CGFloat(Int(size.height) - Int((size.height/3 )*2  )))
                )
            )
            else
        {
            return nil
        }
        return UIImage(cgImage: image)
    }
    
    var bottomHalf: UIImage? {
        guard let cgImage = cgImage,
            let image = cgImage.cropping(to: CGRect(
                origin: CGPoint(x: 0,  y: CGFloat(Int(size.height) - Int(size.height/3))),
                size: CGSize(width: size.width, height: CGFloat(Int(size.height) - Int((size.height/3)*2 )))
                )
            )
            else
        {
            return nil
        }
        return UIImage(cgImage: image)
    }
    
    
    
    
    var leftHalf: UIImage? {
        guard let cgImage = cgImage, let image = cgImage.cropping(to: CGRect(
            origin: .zero,
            size: CGSize(width: size.width/3, height: size.height)
            )
            )
            else
        {
            return nil
        }
        return UIImage(cgImage: image)
    }
    
    
    var horizontallyMiddleHalf: UIImage? {
        guard let cgImage = cgImage, let image = cgImage.cropping(to: CGRect(
            origin: CGPoint(x: CGFloat(Int(size.width)-Int((size.width/3))-Int((size.width/3))), y: 0),
            size: CGSize(width: CGFloat(Int(size.width)-Int(((size.width/3)*2))), height: size.height)
            )
            )
            else
        {
            return nil
        }
        return UIImage(cgImage: image)
    }
    
    
    
    
    var rightHalf: UIImage? {
        guard let cgImage = cgImage, let image = cgImage.cropping(to: CGRect(
            origin: CGPoint(x: CGFloat(Int(size.width)-Int((size.width/3))), y: 0),
            size: CGSize(width: CGFloat(Int(size.width)-Int(((size.width/3)*2))), height: size.height)
            )
            )
            else
        {
            return nil
        }
        return UIImage(cgImage: image)
    }
}

extension Array {
    mutating func shuffle()
    {
        for _ in 0..<10
        {
            sort { (_,_) in arc4random() < arc4random() }
        }
    }
}











